package com.example.recipes.adapters

import android.view.ActionMode
import android.view.LayoutInflater
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.content.ContextCompat
import androidx.fragment.app.FragmentActivity
import androidx.navigation.Navigation
import androidx.recyclerview.widget.AsyncListDiffer
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import coil.load
import com.example.recipes.R
import com.example.recipes.databinding.FavoriteRecipeRowLayoutBinding
import com.example.recipes.repository.FavoritesEntity
import com.example.recipes.view.FavoriteRecipesFragmentDirections
import com.example.recipes.viewmodel.RecipesViewModel
import com.google.android.material.card.MaterialCardView
import com.google.android.material.snackbar.Snackbar
import org.jsoup.Jsoup

class FavoriteRecipesAdapter(
    private val requireActivity: FragmentActivity,
    private val recipeViewModel: RecipesViewModel
) :
    RecyclerView.Adapter<FavoriteRecipesAdapter.FavoritesRecipesViewHolder>(),
    ActionMode.Callback {

    private lateinit var rootView: View
    private var multiSelection = false
    private var myViewHolders =
        arrayListOf<FavoritesRecipesViewHolder>()

    private lateinit var actionMode: ActionMode
    private var selectedRecipes = arrayListOf<FavoritesEntity>()

    override fun onCreateActionMode(mode: ActionMode?, menu: Menu?): Boolean {
        mode?.menuInflater?.inflate(R.menu.favorites_contextual_menu, menu)
        actionMode = mode!!
        setStatusBarColor(R.color.contextualStatusBarColor)
        return true
    }

    override fun onPrepareActionMode(mode: ActionMode?, menu: Menu?): Boolean {
        return true
    }

    override fun onActionItemClicked(mode: ActionMode?, menuItem: MenuItem?): Boolean {
        if (menuItem?.itemId == R.id.delete_favorite_recipe_menu) {
            selectedRecipes.forEach {
                recipeViewModel.deleteFavoriteRecipe(it)
            }
            showSnackBar("${selectedRecipes.size} recipe(s) removed")
            multiSelection = false
            selectedRecipes.clear()
            actionMode.finish()
        }
        return true
    }

    override fun onDestroyActionMode(mode: ActionMode?) {
        myViewHolders.forEach {
            changeRecipeStyle(it, R.color.cardBackgroundColor, R.color.strokeColor)
        }
        multiSelection = false
        selectedRecipes.clear()
        setStatusBarColor(R.color.statusBarColor)
    }

    private val diffCallback =
        object : DiffUtil.ItemCallback<FavoritesEntity>() {
            override fun areItemsTheSame(
                oldItem: FavoritesEntity,
                newItem: FavoritesEntity
            ) = oldItem.id == newItem.id

            override fun areContentsTheSame(
                oldItem: FavoritesEntity,
                newItem: FavoritesEntity
            ) = oldItem == newItem
        }

    private val differ = AsyncListDiffer(this, diffCallback)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): FavoritesRecipesViewHolder {
        val itemBinding = FavoriteRecipeRowLayoutBinding.inflate(
            LayoutInflater.from(parent.context), parent, false
        )
        return FavoritesRecipesViewHolder(itemBinding)
    }

    override fun onBindViewHolder(holder: FavoritesRecipesViewHolder, position: Int) {
        val favoritesEntity = differ.currentList[position]
        rootView = holder.itemView.rootView
        saveItemStateOnScroll(favoritesEntity, holder)

        myViewHolders.add(holder)
        holder.bind(favoritesEntity)
        holder.itemView.setOnClickListener {
            if (multiSelection) {
                applySelection(holder, favoritesEntity)
            } else {
                Navigation.findNavController(it)
                    .navigate(
                        FavoriteRecipesFragmentDirections
                            .actionFavoriteRecipesFragmentToDetailsActivity(favoritesEntity.result)
                    )
            }
        }

        holder.itemView.setOnLongClickListener {
            if (!multiSelection) {
                multiSelection = true
                requireActivity.startActionMode(this)
                applySelection(holder, favoritesEntity)
                true
            } else {
                applySelection(holder, favoritesEntity)
                true
            }
        }
    }

    override fun getItemCount() = differ.currentList.size

    private fun setStatusBarColor(color: Int) {
        requireActivity.window.statusBarColor =
            ContextCompat.getColor(requireActivity, color)
    }

    private fun applySelection(
        holder: FavoritesRecipesViewHolder,
        currentRecipe: FavoritesEntity
    ) {
        if (selectedRecipes.contains(currentRecipe)) {
            selectedRecipes.remove(currentRecipe)
            changeRecipeStyle(holder, R.color.cardBackgroundColor, R.color.strokeColor)
            applyActionModeTitle()
        } else {
            selectedRecipes.add(currentRecipe)
            changeRecipeStyle(holder, R.color.cardBackgroundLightColor, R.color.colorPrimary)
            applyActionModeTitle()
        }
    }

    private fun changeRecipeStyle(
        holder: FavoritesRecipesViewHolder,
        backgroundColor: Int,
        strokeColor: Int
    ) {
        holder.itemView.apply {
            findViewById<ConstraintLayout>(R.id.favoriteRecipeRowLayout)
                .setBackgroundColor(ContextCompat.getColor(requireActivity, backgroundColor))

            findViewById<MaterialCardView>(R.id.favorite_row_cardView).strokeColor =
                ContextCompat.getColor(
                    requireActivity,
                    strokeColor
                )
        }
    }

    private fun applyActionModeTitle() {
        when (selectedRecipes.size) {
            0 -> {
                actionMode.finish()
                multiSelection = false
            }
            1 -> {
                actionMode.title = "${selectedRecipes.size}  item selected"
            }
            else -> actionMode.title = "${selectedRecipes.size}  items selected"
        }
    }

    private fun showSnackBar(message: String) {
        Snackbar.make(rootView, message, Snackbar.LENGTH_SHORT)
            .setAction("Okay") {}
            .show()
    }

    fun setFavoriteRecipesData(newData: List<FavoritesEntity>) {
        differ.submitList(newData)
    }

    fun clearContextualActionMode() {
        if (this::actionMode.isInitialized) {
            actionMode.finish()
        }
    }

    /* fixing recycler view behavior when there's a favorite recipe selected for deletion */
    private fun saveItemStateOnScroll(
        currentRecipe: FavoritesEntity,
        holder: FavoritesRecipesViewHolder
    ) {
        if (selectedRecipes.contains(currentRecipe)) {
            changeRecipeStyle(holder, R.color.cardBackgroundLightColor, R.color.colorPrimary)
        } else {
            changeRecipeStyle(holder, R.color.cardBackgroundColor, R.color.strokeColor)
        }
    }

    class FavoritesRecipesViewHolder(itemBinding: FavoriteRecipeRowLayoutBinding) :
        RecyclerView.ViewHolder(itemBinding.root) {
        private val favoriteImage = itemBinding.favoriteRecipeImageView
        private val favoriteTitle = itemBinding.favoriteTitleTextView
        private val favoriteDescription = itemBinding.favoriteDescriptionTextView
        private val favoriteLikesIcon = itemBinding.favoriteHeartImageView
        private val favoriteLikesCount = itemBinding.favoriteHeartTextView
        private val favoriteClockIcon = itemBinding.favoriteClockImageView
        private val favoriteClockText = itemBinding.favoriteClockTextView
        private val favoriteVeganIcon = itemBinding.favoriteLeafImageView
        private val favoriteVeganText = itemBinding.favoriteLeafTextView

        private fun applyColors(view: View, color: Int) {
            when (view) {
                is TextView -> view.setTextColor(ContextCompat.getColor(view.context, color))
                is ImageView -> view.setColorFilter(ContextCompat.getColor(view.context, color))
            }
        }

        fun bind(favoritesEntity: FavoritesEntity) {
            favoriteImage.load(favoritesEntity.result.image) {
                crossfade(600)
                error(R.drawable.ic_launcher_foreground)
            }
            favoriteTitle.text = favoritesEntity.result.title
            favoriteDescription.text = Jsoup.parse(favoritesEntity.result.summary).text()
            favoriteLikesCount.text = favoritesEntity.result.aggregateLikes.toString()
            favoriteClockText.text = favoritesEntity.result.readyInMinutes.toString()
            favoriteVeganText.text = favoritesEntity.result.vegan.toString()

            favoriteLikesIcon.setOnClickListener {
                applyColors(it, R.color.red)
            }

            favoriteClockIcon.setOnClickListener {
                applyColors(it, R.color.yellow)
            }

            favoriteVeganIcon.setOnClickListener {
                applyColors(it, R.color.green)
            }
        }
    }
}