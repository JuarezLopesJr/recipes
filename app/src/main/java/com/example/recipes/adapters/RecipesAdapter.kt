package com.example.recipes.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.core.content.ContextCompat
import androidx.navigation.Navigation
import androidx.recyclerview.widget.AsyncListDiffer
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import coil.load
import com.example.recipes.R
import com.example.recipes.databinding.RecipeRowLayoutBinding
import com.example.recipes.model.FoodRecipe
import com.example.recipes.model.RecipeResult
import com.example.recipes.view.RecipesFragmentDirections
import org.jsoup.Jsoup

class RecipesAdapter : RecyclerView.Adapter<RecipesAdapter.RecipesViewHolder>() {
    //    private var result = emptyList<RecipeResult>()
    private val diffCallback =
        object : DiffUtil.ItemCallback<RecipeResult>() {
            override fun areItemsTheSame(oldItem: RecipeResult, newItem: RecipeResult) =
                oldItem.recipeId == newItem.recipeId

            override fun areContentsTheSame(oldItem: RecipeResult, newItem: RecipeResult) =
                oldItem == newItem
        }

    private val differ = AsyncListDiffer(this, diffCallback)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecipesViewHolder {
        val itemBinding = RecipeRowLayoutBinding.inflate(
            LayoutInflater.from(parent.context), parent, false
        )
        return RecipesViewHolder(itemBinding)
    }

    override fun onBindViewHolder(holder: RecipesViewHolder, position: Int) {
        val recipes = differ.currentList[position]
        holder.bind(recipes)
        /* setting click listener for each list view */
        holder.itemView.setOnClickListener {
            Navigation.findNavController(it)
                .navigate(
                    RecipesFragmentDirections.actionRecipesFragmentToDetailsActivity(recipes)
                )
        }
    }

    override fun getItemCount() = differ.currentList.size

    fun setData(newData: FoodRecipe) {
        differ.submitList(newData.recipeResults)
        /*val recipesDiffUtil = RecipesDiffUtil(result, newData.recipeResults)
        val diffUtilResult = DiffUtil.calculateDiff(recipesDiffUtil)
        result = newData.recipeResults
        diffUtilResult.dispatchUpdatesTo(this)*/
    }

    class RecipesViewHolder(itemBinding: RecipeRowLayoutBinding) :
        RecyclerView.ViewHolder(itemBinding.root) {

        private val recipeImage = itemBinding.rowImage
        private val recipeTitle = itemBinding.rowRecipeTitle
        private val recipeDescription = itemBinding.rowRecipeDescription
        private val recipeLikeCount = itemBinding.rowLikeBtnValue
        private val recipeLikeIcon = itemBinding.rowLikeBtn
        private val recipeTimeCount = itemBinding.rowTimeBtnValue
        private val recipeTimeIcon = itemBinding.rowTimeBtn
        private val recipeVeganValue = itemBinding.rowLeafBtnValue
        private val recipeVeganIcon = itemBinding.rowLeafBtn


        private fun applyColors(view: View, color: Int) {
            when (view) {
                is TextView -> view.setTextColor(ContextCompat.getColor(view.context, color))
                is ImageView -> view.setColorFilter(ContextCompat.getColor(view.context, color))
            }
        }

        fun bind(recipeResult: RecipeResult) {
            recipeTitle.text = recipeResult.title
            recipeDescription.text = Jsoup.parse(recipeResult.summary).text()
            recipeLikeCount.text = recipeResult.aggregateLikes.toString()
            recipeTimeCount.text = recipeResult.readyInMinutes.toString()
            recipeVeganValue.text = recipeResult.vegan.toString()

            recipeImage.load(recipeResult.image) {
                crossfade(600)
                error(R.drawable.ic_launcher_foreground)
            }
            recipeLikeIcon.setOnClickListener {
                applyColors(it, R.color.red)
            }

            recipeTimeIcon.setOnClickListener {
                applyColors(it, R.color.yellow)
            }

            recipeVeganIcon.setOnClickListener {
                applyColors(it, R.color.green)
            }
        }
    }
}