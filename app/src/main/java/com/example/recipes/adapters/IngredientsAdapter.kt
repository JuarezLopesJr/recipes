package com.example.recipes.adapters

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.AsyncListDiffer
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import coil.load
import com.example.recipes.R
import com.example.recipes.databinding.IngredientsRowLayoutBinding
import com.example.recipes.model.ExtendedIngredient
import com.example.recipes.utils.Constants.Companion.BASE_IMAGE_URL

class IngredientsAdapter : RecyclerView.Adapter<IngredientsAdapter.IngredientsViewHolder>() {

    private val diffCallback =
        object : DiffUtil.ItemCallback<ExtendedIngredient>() {
            override fun areItemsTheSame(
                oldItem: ExtendedIngredient,
                newItem: ExtendedIngredient
            ) = oldItem.name == newItem.name

            override fun areContentsTheSame(
                oldItem: ExtendedIngredient,
                newItem: ExtendedIngredient
            ) = oldItem == newItem
        }

    private val differ = AsyncListDiffer(this, diffCallback)

    override fun onBindViewHolder(holder: IngredientsViewHolder, position: Int) {
        val ingredients = differ.currentList[position]
        holder.bind(ingredients)
    }

    override fun getItemCount() = differ.currentList.size

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): IngredientsViewHolder {
        val itemBinding = IngredientsRowLayoutBinding.inflate(
            LayoutInflater.from(parent.context), parent, false
        )
        return IngredientsViewHolder(itemBinding)
    }

    fun setIngredientData(newData: List<ExtendedIngredient>) {
        differ.submitList(newData)
    }

    class IngredientsViewHolder(itemBinding: IngredientsRowLayoutBinding) :
        RecyclerView.ViewHolder(itemBinding.root) {

        private val ingredientImage = itemBinding.ingredientImageView
        private val ingredientName = itemBinding.ingredientName
        private val ingredientAmount = itemBinding.ingredientAmount
        private val ingredientUnit = itemBinding.ingredientUnit
        private val ingredientConsistency = itemBinding.ingredientConsistency
        private val ingredientOriginal = itemBinding.ingredientOriginal

        fun bind(ingredient: ExtendedIngredient) {
            ingredientImage.load(BASE_IMAGE_URL + ingredient.image) {
                crossfade(600)
                error(R.drawable.ic_launcher_foreground)
            }
            ingredientName.text = ingredient.name.uppercase()
            ingredientAmount.text = ingredient.amount.toString()
            ingredientUnit.text = ingredient.unit
            ingredientConsistency.text = ingredient.consistency
            ingredientOriginal.text = ingredient.original
        }
    }
}