package com.example.recipes.viewmodel

import android.app.Application
import android.content.Context
import android.net.ConnectivityManager
import android.net.NetworkCapabilities
import android.widget.Toast
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.asLiveData
import androidx.lifecycle.viewModelScope
import com.example.recipes.model.FoodJoke
import com.example.recipes.model.FoodRecipe
import com.example.recipes.repository.DataStoreRepository
import com.example.recipes.repository.FavoritesEntity
import com.example.recipes.repository.FoodJokeEntity
import com.example.recipes.repository.RecipesEntity
import com.example.recipes.repository.Repository
import com.example.recipes.utils.Constants.Companion.API_KEY
import com.example.recipes.utils.Constants.Companion.DEFAULT_DIET_TYPE
import com.example.recipes.utils.Constants.Companion.DEFAULT_MEAL_TYPE
import com.example.recipes.utils.Constants.Companion.DEFAULT_RECIPES_NUMBER
import com.example.recipes.utils.Constants.Companion.NETWORK_OFFLINE
import com.example.recipes.utils.Constants.Companion.NETWORK_ONLINE
import com.example.recipes.utils.Constants.Companion.QUERY_ADD_RECIPE_INFORMATION
import com.example.recipes.utils.Constants.Companion.QUERY_API_KEY
import com.example.recipes.utils.Constants.Companion.QUERY_DIET
import com.example.recipes.utils.Constants.Companion.QUERY_FILL_INGREDIENTS
import com.example.recipes.utils.Constants.Companion.QUERY_NUMBER
import com.example.recipes.utils.Constants.Companion.QUERY_SEARCH
import com.example.recipes.utils.Constants.Companion.QUERY_TYPE
import com.example.recipes.utils.NetworkResult
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch
import retrofit2.Response
import javax.inject.Inject

@HiltViewModel
class RecipesViewModel @Inject constructor(
    private val repository: Repository,
    private val dataStoreRepository: DataStoreRepository,
    application: Application
) : AndroidViewModel(application) {

    private var mealType = DEFAULT_MEAL_TYPE
    private var dietType = DEFAULT_DIET_TYPE
    var networkStatus = false
    var backOnline = false

    val readMealAndDietType =
        dataStoreRepository.readMealAndDietTypeRepository

    val readBackOnline =
        dataStoreRepository.readBackOnlineRepository.asLiveData()

    var recipesResponse: MutableLiveData<NetworkResult<FoodRecipe>> = MutableLiveData()
    var searchRecipesResponse: MutableLiveData<NetworkResult<FoodRecipe>> = MutableLiveData()
    var foodJokeResponse: MutableLiveData<NetworkResult<FoodJoke>> = MutableLiveData()

    /* transforming Flow to LiveData */
    val readRecipes = repository.local.readRecipesFromLocal().asLiveData()
    val readFavoriteRecipes =
        repository.local.readFavoriteRecipeFromLocal().asLiveData()

    val readFoodJoke =
        repository.local.readFoodJokesFromLocal().asLiveData()

    fun getRecipes(queries: Map<String, String>) = viewModelScope.launch {
        getRecipesSafeCall(queries)
    }

    fun searchRecipes(searchQuery: Map<String, String>) = viewModelScope.launch {
        searchRecipesSafeCall(searchQuery)
    }

    fun saveMealAndDietType(
        mealType: String,
        mealTypeId: Int,
        dietType: String,
        dietTypeId: Int
    ) = viewModelScope.launch(Dispatchers.IO) {
        dataStoreRepository
            .saveMealAndDietTypeRepository(mealType, mealTypeId, dietType, dietTypeId)
    }


    private fun saveBackOnline(state: Boolean) {
        viewModelScope.launch(Dispatchers.IO) {
            dataStoreRepository.saveBackOnlineRepository(state)
        }
    }

    fun applyQueries(): HashMap<String, String> {
        val queries: HashMap<String, String> = HashMap()

        viewModelScope.launch(Dispatchers.IO) {
            readMealAndDietType.collect {
                mealType = it.selectedMealType
                dietType = it.selectedDietType
            }
        }

        queries[QUERY_ADD_RECIPE_INFORMATION] = "true"
        queries[QUERY_NUMBER] = DEFAULT_RECIPES_NUMBER
        queries[QUERY_API_KEY] = API_KEY
        queries[QUERY_DIET] = dietType
        queries[QUERY_TYPE] = mealType
        queries[QUERY_FILL_INGREDIENTS] = "true"

        return queries
    }

    fun applySearchQuery(searchQuery: String): HashMap<String, String> {
        val queries: HashMap<String, String> = HashMap()

        queries[QUERY_ADD_RECIPE_INFORMATION] = "true"
        queries[QUERY_NUMBER] = DEFAULT_RECIPES_NUMBER
        queries[QUERY_API_KEY] = API_KEY
        queries[QUERY_FILL_INGREDIENTS] = "true"
        queries[QUERY_SEARCH] = searchQuery

        return queries
    }

    private suspend fun getRecipesSafeCall(queries: Map<String, String>) {
        recipesResponse.value = NetworkResult.Loading()
        if (hasInternetConnection()) {
            try {
                val response = repository.remote.getRecipesFromRemote(queries)
                recipesResponse.value = handleFoodRecipesResponse(response)

                val foodRecipe = recipesResponse.value!!.data
                if (foodRecipe != null) {
                    offlineCacheRecipes(foodRecipe)
                }
            } catch (e: Exception) {
                e.printStackTrace()
                recipesResponse.value = NetworkResult.Error(
                    "Recipes not found: ${e.localizedMessage}"
                )
            }
        } else {
            recipesResponse.value = NetworkResult.Error("No internet connection")
        }
    }

    private suspend fun searchRecipesSafeCall(searchQuery: Map<String, String>) {
        searchRecipesResponse.value = NetworkResult.Loading()
        if (hasInternetConnection()) {
            try {
                val response = repository.remote.searchRecipesFromRemote(searchQuery)
                searchRecipesResponse.value = handleFoodRecipesResponse(response)

            } catch (e: Exception) {
                e.printStackTrace()
                searchRecipesResponse.value = NetworkResult.Error(
                    "Recipes not found: ${e.localizedMessage}"
                )
            }
        } else {
            searchRecipesResponse.value = NetworkResult.Error("No internet connection")
        }
    }

    private suspend fun getFoodJokeSafeCall(apiKey: String) {
        foodJokeResponse.value = NetworkResult.Loading()
        if (hasInternetConnection()) {
            try {
                val response = repository.remote.getFoodJokeFromRemote(apiKey)
                foodJokeResponse.value = handleFoodJokeResponse(response)

                val foodJoke = foodJokeResponse.value!!.data
                if (foodJoke != null) {
                    offlineCacheFoodJokes(foodJoke)
                }
            } catch (e: Exception) {
                e.printStackTrace()
                foodJokeResponse.value = NetworkResult.Error(
                    "No internet connection"
                )
            }
        } else {
            foodJokeResponse.value = NetworkResult.Error("No internet connection")
        }
    }

    private fun offlineCacheRecipes(foodRecipe: FoodRecipe) {
        val recipesEntity = RecipesEntity(foodRecipe)
        insertRecipes(recipesEntity)
    }

    private fun offlineCacheFoodJokes(foodJoke: FoodJoke) {
        val foodJokeEntity = FoodJokeEntity(foodJoke)
        insertFoodJoke(foodJokeEntity)
    }

    fun insertRecipes(recipesEntity: RecipesEntity) =
        viewModelScope.launch(Dispatchers.IO) {
            repository.local.insertRecipesFromLocal(recipesEntity)
        }

    fun insertFavoriteRecipe(favoritesEntity: FavoritesEntity) =
        viewModelScope.launch(Dispatchers.IO) {
            repository.local.insertFavoriteRecipeFromLocal(favoritesEntity)
        }

    fun deleteFavoriteRecipe(favoritesEntity: FavoritesEntity) =
        viewModelScope.launch(Dispatchers.IO) {
            repository.local.deleteFavoriteRecipeFromLocal(favoritesEntity)
        }

    fun deleteAllFavoriteRecipe() =
        viewModelScope.launch(Dispatchers.IO) {
            repository.local.deleteAllFavoriteRecipesFromLocal()
        }

    fun insertFoodJoke(foodJokeEntity: FoodJokeEntity) =
        viewModelScope.launch(Dispatchers.IO) {
            repository.local.insertFoodJokeFromLocal(foodJokeEntity)
        }

    fun getFoodJoke(apiKey: String) = viewModelScope.launch {
        getFoodJokeSafeCall(apiKey)
    }

    private fun hasInternetConnection(): Boolean {
        val connectivityManager = getApplication<Application>().getSystemService(
            Context.CONNECTIVITY_SERVICE
        ) as ConnectivityManager

        val activeNetwork = connectivityManager.activeNetwork ?: return false
        val capabilities = connectivityManager
            .getNetworkCapabilities(activeNetwork) ?: return false

        return when {
            capabilities.hasTransport(NetworkCapabilities.TRANSPORT_WIFI) -> true
            capabilities.hasTransport(NetworkCapabilities.TRANSPORT_CELLULAR) -> true
            capabilities.hasTransport(NetworkCapabilities.TRANSPORT_ETHERNET) -> true
            else -> false
        }
    }

    /* treating network request possible outcomes */
    private fun handleFoodRecipesResponse(
        response: Response<FoodRecipe>
    ): NetworkResult<FoodRecipe> {
        return when {
            response.message().toString().contains("timeout") -> {
                NetworkResult.Error("Timeout")
            }
            response.code() == 402 -> {
                NetworkResult.Error("API key limited")
            }
            response.body()?.recipeResults.isNullOrEmpty() -> {
                NetworkResult.Error("Recipe not found")
            }
            response.isSuccessful -> {
                NetworkResult.Success(response.body()!!)
            }
            else -> NetworkResult.Error(response.message())
        }
    }

    private fun handleFoodJokeResponse(
        response: Response<FoodJoke>
    ): NetworkResult<FoodJoke> {
        return when {
            response.message().toString().contains("timeout") -> {
                NetworkResult.Error("Timeout")
            }
            response.code() == 402 -> {
                NetworkResult.Error("API key limited")
            }
            response.isSuccessful -> {
                NetworkResult.Success(response.body()!!)
            }
            else -> NetworkResult.Error(response.message())
        }
    }

    fun showNetworkStatus() {
        when {
            !networkStatus -> {
                Toast.makeText(getApplication(), NETWORK_OFFLINE, Toast.LENGTH_SHORT)
                    .show()
                saveBackOnline(true)
            }
            networkStatus -> {
                if (backOnline) {
                    Toast.makeText(getApplication(), NETWORK_ONLINE, Toast.LENGTH_SHORT)
                        .show()
                    saveBackOnline(false)
                }
            }
        }
    }
}