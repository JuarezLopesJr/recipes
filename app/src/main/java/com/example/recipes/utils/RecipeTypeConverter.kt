package com.example.recipes.utils

import androidx.room.TypeConverter
import com.example.recipes.model.FoodRecipe
import com.example.recipes.model.RecipeResult
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken

class RecipeTypeConverter {
    private var gson = Gson()

    @TypeConverter
    fun foodRecipeToString(foodRecipe: FoodRecipe): String {
        return gson.toJson(foodRecipe)
    }

    @TypeConverter
    fun stringToFoodRecipe(data: String): FoodRecipe {
        val listType = object : TypeToken<FoodRecipe>() {}.type
        return gson.fromJson(data, listType)
    }

    @TypeConverter
    fun recipeResultToString(recipeResult: RecipeResult): String {
        return gson.toJson(recipeResult)
    }

    @TypeConverter
    fun stringToRecipeResult(data: String): RecipeResult {
        val listType = object : TypeToken<RecipeResult>() {}.type
        return gson.fromJson(data, listType)
    }
}