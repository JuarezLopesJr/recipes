package com.example.recipes.utils

import android.widget.ImageView
import coil.load
import com.example.recipes.R

fun loadImage(imageView: ImageView, url: String) {
    imageView.load(url) {
        crossfade(600)
        error(R.drawable.ic_cloud_off)
    }
}