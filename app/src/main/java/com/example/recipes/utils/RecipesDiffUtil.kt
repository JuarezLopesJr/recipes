package com.example.recipes.utils

import androidx.recyclerview.widget.DiffUtil
import com.example.recipes.model.RecipeResult

class RecipesDiffUtil(
    private val oldList: List<RecipeResult>,
    private val newList: List<RecipeResult>
) : DiffUtil.Callback() {

    override fun getOldListSize() = oldList.size

    override fun getNewListSize() = newList.size

    override fun areItemsTheSame(oldItemPosition: Int, newItemPosition: Int) =
        oldList[oldItemPosition] === newList[newItemPosition]

    override fun areContentsTheSame(oldItemPosition: Int, newItemPosition: Int) =
        oldList[oldItemPosition] == newList[newItemPosition]
}