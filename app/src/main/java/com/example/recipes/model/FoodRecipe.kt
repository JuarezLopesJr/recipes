package com.example.recipes.model

data class FoodRecipe(
    val recipeResults: List<RecipeResult>
)
