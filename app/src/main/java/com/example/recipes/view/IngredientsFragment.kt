package com.example.recipes.view

import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.recipes.R
import com.example.recipes.adapters.IngredientsAdapter
import com.example.recipes.databinding.FragmentIngredientsBinding
import com.example.recipes.model.RecipeResult
import com.example.recipes.utils.Constants.Companion.RECIPE_RESULT_KEY
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class IngredientsFragment : Fragment(R.layout.fragment_ingredients) {
    private var fragmentIngredientsBinding: FragmentIngredientsBinding? = null
    private val ingredientsAdapter by lazy { IngredientsAdapter() }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val binding = FragmentIngredientsBinding.bind(view)
        fragmentIngredientsBinding = binding
        setupRecyclerView()
        setupViews()
    }

    override fun onDestroy() {
        fragmentIngredientsBinding = null
        super.onDestroy()
    }

    private fun setupRecyclerView() =
        fragmentIngredientsBinding!!.ingredientsRecyclerview.apply {
            adapter = ingredientsAdapter
            layoutManager = LinearLayoutManager(context)
        }

    private fun setupViews() {
        val args = arguments
        val myBundle: RecipeResult? = args?.getParcelable(RECIPE_RESULT_KEY)

        myBundle?.extendedIngredients?.let {
            ingredientsAdapter.setIngredientData(it)
        }
    }
}