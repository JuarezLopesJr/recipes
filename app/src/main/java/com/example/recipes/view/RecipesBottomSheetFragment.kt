package com.example.recipes.view

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.viewModels
import androidx.lifecycle.asLiveData
import androidx.navigation.fragment.findNavController
import com.example.recipes.databinding.FragmentRecipesBottomSheetBinding
import com.example.recipes.utils.Constants.Companion.DEFAULT_DIET_TYPE
import com.example.recipes.utils.Constants.Companion.DEFAULT_MEAL_TYPE
import com.example.recipes.viewmodel.RecipesViewModel
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import com.google.android.material.chip.Chip
import com.google.android.material.chip.ChipGroup
import dagger.hilt.android.AndroidEntryPoint
import java.util.*

@AndroidEntryPoint
class RecipesBottomSheetFragment : BottomSheetDialogFragment() {
    private var fragmentBottomRecipesBinding: FragmentRecipesBottomSheetBinding? = null
    private val viewModel: RecipesViewModel by viewModels()

    private var mealTypeChip = DEFAULT_MEAL_TYPE
    private var mealTypeChipId = 0
    private var dietTypeChip = DEFAULT_DIET_TYPE
    private var dietTypeChipId = 0

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        val binding =
            FragmentRecipesBottomSheetBinding.inflate(inflater, container, false)

        /* reading the values from DataStore inside the bottomview */
        viewModel.readMealAndDietType.asLiveData().observe(viewLifecycleOwner, {
            mealTypeChip = it.selectedMealType
            dietTypeChip = it.selectedDietType

            updateChip(it.selectedMealTypeId, binding.mealTypeChipGroup)
            updateChip(it.selectedDietTypeId, binding.dietTypeChipGroup)
        })

        /* getting the values from selected chip */
        binding.mealTypeChipGroup.setOnCheckedChangeListener { group, checkedId ->
            val chipMeal = group.findViewById<Chip>(checkedId)
            val selectedMealType = chipMeal.text.toString().lowercase(Locale.ROOT)
            mealTypeChip = selectedMealType
            mealTypeChipId = checkedId
        }

        /* getting the values from selected chip */
        binding.dietTypeChipGroup.setOnCheckedChangeListener { group, checkedId ->
            val chipDiet = group.findViewById<Chip>(checkedId)
            val selectedDietType = chipDiet.text.toString().lowercase(Locale.ROOT)
            dietTypeChip = selectedDietType
            dietTypeChipId = checkedId
        }

        /* saving the values from chips in DataStore */
        binding.btnApply.setOnClickListener {
            viewModel.saveMealAndDietType(
                mealTypeChip,
                mealTypeChipId,
                dietTypeChip,
                dietTypeChipId
            )
            val action =
                RecipesBottomSheetFragmentDirections
                    .actionRecipesBottomSheetFragmentToRecipesFragment(true)

            findNavController()
                .navigate(action)
        }

        return binding.root
    }

    override fun onDestroy() {
        fragmentBottomRecipesBinding = null
        super.onDestroy()
    }

    private fun updateChip(chipId: Int, chipGroup: ChipGroup) {
        if (chipId != 0) {
            try {
                chipGroup.findViewById<Chip>(chipId).isChecked = true
            } catch (e: Exception) {
                Log.d("BottomSheetFragment", e.message.toString())
            }
        }
    }
}