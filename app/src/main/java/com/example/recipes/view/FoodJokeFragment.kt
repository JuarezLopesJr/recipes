package com.example.recipes.view

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.Menu
import android.view.MenuInflater
import android.view.MenuItem
import android.view.View
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import com.example.recipes.R
import com.example.recipes.databinding.FragmentFoodJokeBinding
import com.example.recipes.utils.Constants.Companion.API_KEY
import com.example.recipes.utils.NetworkResult
import com.example.recipes.viewmodel.RecipesViewModel
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class FoodJokeFragment : Fragment(R.layout.fragment_food_joke) {
    private var fragmentFoodJokeBinding: FragmentFoodJokeBinding? = null
    private val viewModel: RecipesViewModel by viewModels()
    private lateinit var foodJoke: String

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val binding = FragmentFoodJokeBinding.bind(view)
        setHasOptionsMenu(true)
        fragmentFoodJokeBinding = binding
        setupViews()
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        super.onCreateOptionsMenu(menu, inflater)
        inflater.inflate(R.menu.food_joke_menu, menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when(item.itemId) {
            R.id.share_food_joke_menu -> {
                val shareIntent = Intent(Intent.ACTION_SEND)
                shareIntent.setType("text/plain")
                    .putExtra(Intent.EXTRA_TEXT, foodJoke)

                startActivity(shareIntent)
            }
        }
        return super.onOptionsItemSelected(item)
    }

    override fun onDestroy() {
        fragmentFoodJokeBinding = null
        super.onDestroy()
    }

    private fun setupViews() {
        viewModel.getFoodJoke(API_KEY)
        viewModel.foodJokeResponse.observe(viewLifecycleOwner, { response ->
            when (response) {
                is NetworkResult.Success -> {
                    fragmentFoodJokeBinding!!.apply {
                        progressBar.visibility = View.GONE
                        foodJokeTextView.visibility = View.VISIBLE
                        foodJokeTextView.text = response.data?.text
                        foodJokeErrorImageView.visibility = View.GONE
                        foodJokeErrorTextView.visibility = View.GONE
                    }
                    foodJoke = response.data?.text.toString()
                }
                is NetworkResult.Error -> {
                    fragmentFoodJokeBinding!!.apply {
                        progressBar.visibility = View.GONE
                        foodJokeErrorImageView.visibility = View.VISIBLE
                        foodJokeErrorTextView.visibility = View.VISIBLE
                    }
                    loadDataFromCache()
                    Toast.makeText(
                        requireContext(),
                        response.message.toString(),
                        Toast.LENGTH_SHORT
                    ).show()
                }
                is NetworkResult.Loading -> {
                    fragmentFoodJokeBinding!!.apply {
                        progressBar.visibility = View.VISIBLE
                        foodJokeTextView.visibility = View.GONE
                        foodJokeErrorImageView.visibility = View.GONE
                        foodJokeErrorTextView.visibility = View.GONE
                    }
                    Log.d("FoodJokeFragment", "Loading...")
                }
            }
        })
    }

    private fun loadDataFromCache() {
        lifecycleScope.launchWhenResumed {
            viewModel.readFoodJoke.observe(viewLifecycleOwner, { database ->
                if (!database.isNullOrEmpty()) {
                    fragmentFoodJokeBinding!!.foodJokeTextView.text =
                        database[0].result.text

                    foodJoke = database[0].result.text
                }
            })
        }
    }
}