package com.example.recipes.view

import android.os.Bundle
import android.view.Menu
import android.view.MenuInflater
import android.view.MenuItem
import android.view.View
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.recipes.R
import com.example.recipes.adapters.FavoriteRecipesAdapter
import com.example.recipes.databinding.FragmentFavoriteRecipesBinding
import com.example.recipes.viewmodel.RecipesViewModel
import com.google.android.material.snackbar.Snackbar
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class FavoriteRecipesFragment : Fragment(R.layout.fragment_favorite_recipes) {
    private var fragmentFavoriteRecipesBinding: FragmentFavoriteRecipesBinding? = null
    private val viewModel: RecipesViewModel by viewModels()

    private val favoriteRecipeAdapter by
    lazy { FavoriteRecipesAdapter(requireActivity(), viewModel) }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val binding = FragmentFavoriteRecipesBinding.bind(view)
        fragmentFavoriteRecipesBinding = binding
        setHasOptionsMenu(true)
        setupRecyclerView()
        setupView()
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.favorite_recipes_menu, menu)
        super.onCreateOptionsMenu(menu, inflater)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.deleteAll_favorite_recipes_menu -> {
                viewModel.deleteAllFavoriteRecipe()
                Snackbar.make(
                    fragmentFavoriteRecipesBinding!!.root,
                    "All recipes removed",
                    Snackbar.LENGTH_SHORT
                )
                    .setAction("Okay") {}
                    .show()
            }
        }
        return super.onOptionsItemSelected(item)
    }

    override fun onDestroy() {
        fragmentFavoriteRecipesBinding = null
        favoriteRecipeAdapter.clearContextualActionMode()
        super.onDestroy()
    }

    private fun setupRecyclerView() {
        fragmentFavoriteRecipesBinding!!.favoriteRecipesRecyclerView.apply {
            adapter = favoriteRecipeAdapter
            layoutManager = LinearLayoutManager(context)
        }
    }

    private fun setupView() {
        viewModel.readFavoriteRecipes.observe(viewLifecycleOwner, {
            if (it.isEmpty()) {
                fragmentFavoriteRecipesBinding!!.apply {
                    noDataImageView.visibility = View.VISIBLE
                    noDataTextView.visibility = View.VISIBLE
                }
            } else {
                favoriteRecipeAdapter.setFavoriteRecipesData(it)
                fragmentFavoriteRecipesBinding!!.apply {
                    noDataImageView.visibility = View.GONE
                    noDataTextView.visibility = View.GONE
                }
            }
        })
    }
}