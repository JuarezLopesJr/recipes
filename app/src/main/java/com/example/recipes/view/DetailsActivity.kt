package com.example.recipes.view

import android.os.Bundle
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.navigation.navArgs
import com.example.recipes.R
import com.example.recipes.adapters.PagerAdapter
import com.example.recipes.databinding.ActivityDetailsBinding
import com.example.recipes.repository.FavoritesEntity
import com.example.recipes.utils.Constants.Companion.RECIPE_RESULT_KEY
import com.example.recipes.viewmodel.RecipesViewModel
import com.google.android.material.snackbar.Snackbar
import com.google.android.material.tabs.TabLayoutMediator
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class DetailsActivity : AppCompatActivity() {
    private lateinit var binding: ActivityDetailsBinding
    private val args by navArgs<DetailsActivityArgs>()
    private val viewModel: RecipesViewModel by viewModels()

    private lateinit var menuItem: MenuItem
    private var recipeSaved = false
    private var savedRecipeId = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityDetailsBinding.inflate(layoutInflater)
        val view = binding.root

        val fragments = ArrayList<Fragment>()
        fragments.add(OverviewFragment())
        fragments.add(IngredientsFragment())
        fragments.add(InstructionsFragment())
        /* creating the bundle to be used in others files with getParcelable() */
        val resultBundle = Bundle()
        resultBundle.putParcelable(RECIPE_RESULT_KEY, args.recipeResult)

        val adapter = PagerAdapter(
            resultBundle,
            fragments,
            supportFragmentManager,
            lifecycle
        )

        binding.apply {
            setContentView(view)
            setSupportActionBar(toolbar)
            toolbar.setTitleTextColor(
                ContextCompat.getColor(this@DetailsActivity, R.color.white)
            )
            /* enabling back arrow in the toolbar */
            supportActionBar?.setDisplayHomeAsUpEnabled(true)
            /* fixing conflict with motion layout */
            viewPager2.isUserInputEnabled = false
            viewPager2.adapter = adapter

            TabLayoutMediator(tabLayout, viewPager2) { tab, position ->
                val tabTitles = listOf("Overview", "Ingredients", "Instructions")
                tab.text = tabTitles[position]
            }.attach()
        }
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.details_menu, menu)
        menuItem = menu!!.findItem(R.id.save_to_favorites_menu)
        checkSavedRecipe(menuItem)

        return super.onCreateOptionsMenu(menu)
    }

    /* click listener for the back arrow button */
    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            android.R.id.home -> finish()

            R.id.save_to_favorites_menu -> {
                if (!recipeSaved) {
                    saveToFavorites(item)
                } else {
                    removeFromFavorites(item)
                }
            }
        }
        return super.onOptionsItemSelected(item)
    }

    override fun onDestroy() {
        super.onDestroy()
        /* persisting yellow star to multiples favorite recipes */
        changeMenuIconColor(menuItem, R.color.white)
    }

    private fun checkSavedRecipe(item: MenuItem) {
        viewModel.readFavoriteRecipes.observe(this, {
            try {
                for (savedRecipe in it) {
                    if (savedRecipe.result.recipeId == args.recipeResult.recipeId) {
                        changeMenuIconColor(item, R.color.yellow)
                        savedRecipeId = savedRecipe.id
                        recipeSaved = true
                    }
                }
            } catch (e: Exception) {
                Log.d("DetailsActivity", e.message.toString())
            }
        })
    }

    private fun saveToFavorites(item: MenuItem) {
        val favoritesEntity = FavoritesEntity(
            0,
            args.recipeResult
        )
        viewModel.insertFavoriteRecipe(favoritesEntity)
        changeMenuIconColor(item, R.color.yellow)
        showSnackBar("Recipe saved")
        recipeSaved = true
    }

    private fun changeMenuIconColor(item: MenuItem, color: Int) {
        item.icon.setTint(ContextCompat.getColor(this, color))
    }

    private fun showSnackBar(message: String) {
        Snackbar.make(
            binding.detailsLayout,
            message,
            Snackbar.LENGTH_SHORT
        ).show()
    }

    private fun removeFromFavorites(item: MenuItem) {
        val favoritesEntity = FavoritesEntity(
            savedRecipeId,
            args.recipeResult
        )
        viewModel.deleteFavoriteRecipe(favoritesEntity)
        changeMenuIconColor(item, R.color.white)
        showSnackBar("Recipe removed from favorites")
        recipeSaved = false
    }
}