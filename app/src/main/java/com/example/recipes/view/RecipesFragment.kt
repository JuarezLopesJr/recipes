package com.example.recipes.view

import android.os.Bundle
import android.util.Log
import android.view.Menu
import android.view.MenuInflater
import android.view.View
import android.widget.Toast
import androidx.appcompat.widget.SearchView
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.recipes.R
import com.example.recipes.adapters.RecipesAdapter
import com.example.recipes.databinding.FragmentRecipesBinding
import com.example.recipes.model.ExtendedIngredient
import com.example.recipes.model.FoodRecipe
import com.example.recipes.model.RecipeResult
import com.example.recipes.repository.RecipesEntity
import com.example.recipes.utils.NetworkListener
import com.example.recipes.utils.NetworkResult
import com.example.recipes.utils.observeOnce
import com.example.recipes.viewmodel.RecipesViewModel
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch

@AndroidEntryPoint
class RecipesFragment : Fragment(R.layout.fragment_recipes), SearchView.OnQueryTextListener {
    private var fragmentRecipesBinding: FragmentRecipesBinding? = null
    private val recipeAdapter by lazy { RecipesAdapter() }
    private val viewModel: RecipesViewModel by viewModels()
    private val args by navArgs<RecipesFragmentArgs>()
    private lateinit var networkListener: NetworkListener

    val summary =
        "Need a <b>gluten free and dairy free main course</b>? African Chicken Peanut Stew could be a tremendous recipe to try. This recipe makes 1 servings with <b>1377 calories</b>, <b>75g of protein</b>, and <b>102g of fat</b> each. For <b>$3.87 per serving</b>, this recipe <b>covers 62%</b> of your daily requirements of vitamins and minerals. It can be enjoyed any time, but it is especially good for <b>Autumn</b>. From preparation to the plate, this recipe takes approximately <b>45 minutes</b>. 124 people have tried and liked this recipe. Head to the store and pick up bell peppers, tomato, pepper, and a few other things to make it today. To use up the tomato you could follow this main course with the <a href=\"https://spoonacular.com/recipes/pink-peony-popcorn-balls-129348\">Pink Peony Popcorn Balls</a> as a dessert. All things considered, we decided this recipe <b>deserves a spoonacular score of 99%</b>. This score is super. Try <a href=\"https://spoonacular.com/recipes/african-chicken-peanut-stew-245461\">African Chicken Peanut Stew</a>, <a href=\"https://spoonacular.com/recipes/west-african-peanut-chicken-stew-163315\">West African Peanut-Chicken Stew</a>, and <a href=\"https://spoonacular.com/recipes/one-pot-african-peanut-stew-854978\">One-Pot African Peanut Stew</a> for similar recipes."
    val extendedIngredient = listOf(
        ExtendedIngredient(
            1.0,
            "solid",
            "bell-pepper-orange.png",
            "bell peppers",
            "Bell Peppers for garnishing",
            "grams"
        )
    )

    val recipeResult = listOf(
        RecipeResult(
            124,
            false,
            true,
            extendedIngredient,
            true,
            716268,
            "https://spoonacular.com/recipeImages/716268-312x231.jpg",
            45,
            "Afrolems",
            "http://www.afrolems.com/2014/03/18/african-chicken-peanut-stew-recipe/",
            summary,
            "African Chicken Peanut Stew",
            false,
            false,
            true
        )
    )

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val binding = FragmentRecipesBinding.bind(view)
        fragmentRecipesBinding = binding
        setHasOptionsMenu(true)
        setupRecyclerView()

        viewModel.apply {
            readBackOnline.observe(viewLifecycleOwner, {
                backOnline = it
            })
            insertRecipes(RecipesEntity(FoodRecipe(recipeResult)))
        }

        lifecycleScope.launch {
            networkListener = NetworkListener()
            networkListener.checkNetworkAvailability(requireContext())
                .collect {
                    Log.d("NetworkListener", it.toString())
                    viewModel.apply {
                        networkStatus = it
                        showNetworkStatus()
                        readDatabase()
                    }
                }
        }

        binding.fabRecipes.setOnClickListener {
            if (viewModel.networkStatus) {
                findNavController().navigate(R.id.action_recipesFragment_to_recipesBottomSheetFragment)
            } else {
                viewModel.showNetworkStatus()
            }
        }
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        super.onCreateOptionsMenu(menu, inflater)
        inflater.inflate(R.menu.recipes_menu, menu)

        val search = menu.findItem(R.id.menu_search)
        val searchView = search.actionView as? SearchView
        searchView?.apply {
            isSubmitButtonEnabled = true
            setOnQueryTextListener(this@RecipesFragment)
        }
    }

    override fun onQueryTextSubmit(query: String?): Boolean {
        query?.let { searchApiData(query) }
        return true
    }

    /* call for every character typed */
    override fun onQueryTextChange(newText: String?): Boolean {
        return true
    }

    override fun onDestroy() {
        fragmentRecipesBinding = null
        super.onDestroy()
    }

    private fun setupRecyclerView() =
        fragmentRecipesBinding!!.recyclerView.apply {
            adapter = recipeAdapter
            layoutManager = LinearLayoutManager(context)
            showShimmer()
        }

    private fun showShimmerEffect() {
        fragmentRecipesBinding?.apply {
            recyclerView.showShimmer()
        }
    }

    private fun hideShimmerEffect() {
        fragmentRecipesBinding?.apply {
            recyclerView.hideShimmer()
        }
    }

    /* observeOnce extension function is to avoid multiple call to the DB */
    private fun readDatabase() {
        /* launchWhenResumed() to avoid crashes when enabling internet outside
           RecipesFragment, because of the lifecycle state  */
        lifecycleScope.launchWhenResumed {
            viewModel.readRecipes.observeOnce(viewLifecycleOwner, { database ->
                /* checking if the flow is not coming from bottomsheetdialog to get data from DB
                    otherwise the data will be fetched from the API */
                if (database.isNotEmpty() && !args.backFromBottomSheet) {
                    Log.d("recipeFragment", "database called")
                    recipeAdapter.setData(database[0].foodRecipe)
                    hideShimmerEffect()
                } else {
                    requestApiData()
                }
            })
        }
    }

    private fun requestApiData() {
        Log.d("recipeFragment", "api called")
        viewModel.getRecipes(viewModel.applyQueries())
        viewModel.recipesResponse.observe(viewLifecycleOwner, { response ->
            when (response) {
                is NetworkResult.Success -> {
                    hideShimmerEffect()
                    fragmentRecipesBinding?.apply {
                        noWifi.visibility = View.GONE
                        noWifiTxt.visibility = View.GONE
                    }
                    response.data?.let { recipeAdapter.setData(it) }
                }
                is NetworkResult.Error -> {
                    hideShimmerEffect()
                    fragmentRecipesBinding?.apply {
                        noWifi.visibility = View.VISIBLE
                        noWifiTxt.visibility = View.VISIBLE
                        noWifiTxt.text = response.message.toString()
                    }
                    /* load from cache in case api fails */
                    loadDataFromCache()
                    Toast.makeText(context, response.message.toString(), Toast.LENGTH_SHORT).show()
                }
                is NetworkResult.Loading -> {
                    fragmentRecipesBinding?.apply {
                        noWifi.visibility = View.GONE
                        noWifiTxt.visibility = View.GONE
                    }
                    showShimmerEffect()
                }
            }
        })
    }

    private fun searchApiData(searchQuery: String) {
        showShimmerEffect()
        viewModel.searchRecipes(viewModel.applySearchQuery(searchQuery))
        viewModel.searchRecipesResponse.observe(viewLifecycleOwner, { response ->
            when (response) {
                is NetworkResult.Success -> {
                    hideShimmerEffect()
                    val foodRecipe = response.data
                    foodRecipe?.let {
                        recipeAdapter.setData(it)
                    }
                }
                is NetworkResult.Error -> {
                    hideShimmerEffect()
                    loadDataFromCache()
                    Toast.makeText(
                        requireContext(),
                        response.message.toString(),
                        Toast.LENGTH_SHORT
                    ).show()
                }
                is NetworkResult.Loading -> {
                    showShimmerEffect()
                }
            }
        })
    }

    private fun loadDataFromCache() {
        lifecycleScope.launch {
            viewModel.readRecipes.observe(viewLifecycleOwner, { database ->
                if (database.isNotEmpty()) {
                    recipeAdapter.setData(database[0].foodRecipe)
                }
            })
        }
    }
}
