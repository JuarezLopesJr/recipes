package com.example.recipes.view

import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import com.example.recipes.R
import com.example.recipes.databinding.FragmentInstructionsBinding
import com.example.recipes.model.RecipeResult
import com.example.recipes.utils.Constants.Companion.RECIPE_RESULT_KEY


class InstructionsFragment : Fragment(R.layout.fragment_instructions) {
    private var fragmentInstructionsBinding: FragmentInstructionsBinding? = null

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val binding = FragmentInstructionsBinding.bind(view)
        fragmentInstructionsBinding = binding
        setupViews()
    }

    override fun onDestroy() {
        fragmentInstructionsBinding = null
        super.onDestroy()
    }

    private fun setupViews() {
        val args = arguments
        val myBundle: RecipeResult? = args?.getParcelable(RECIPE_RESULT_KEY)

        fragmentInstructionsBinding!!.instructionsWebView.apply {
            loadUrl(myBundle!!.sourceUrl)
        }
    }
}