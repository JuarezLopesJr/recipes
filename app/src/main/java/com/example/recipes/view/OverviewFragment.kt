package com.example.recipes.view

import android.os.Bundle
import android.view.View
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import coil.load
import com.example.recipes.R
import com.example.recipes.databinding.FragmentOverviewBinding
import com.example.recipes.model.RecipeResult
import com.example.recipes.utils.Constants.Companion.RECIPE_RESULT_KEY
import org.jsoup.Jsoup

class OverviewFragment : Fragment(R.layout.fragment_overview) {
    private var fragmentOverviewBinding: FragmentOverviewBinding? = null

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val binding = FragmentOverviewBinding.bind(view)
        fragmentOverviewBinding = binding

        setupViews()
    }

    override fun onDestroy() {
        fragmentOverviewBinding = null
        super.onDestroy()
    }

    private fun setupViews() {
        val args = arguments
        val myBundle: RecipeResult? = args?.getParcelable(RECIPE_RESULT_KEY)

        fragmentOverviewBinding?.apply {
            mainImageView.load(myBundle?.image)
            titleTextView.text = myBundle?.title
            likesTextView.text = myBundle?.aggregateLikes.toString()
            timeTextView.text = myBundle?.readyInMinutes.toString()
            summaryTextView.text = Jsoup.parse(myBundle?.summary).text()

            if (myBundle?.vegetarian == true) {
                vegetarianImageView.setColorFilter(
                    ContextCompat.getColor(requireContext(), R.color.green)
                )
                vegetarianTextView.setTextColor(
                    ContextCompat.getColor(requireContext(), R.color.green)
                )
            }

            if (myBundle?.vegan == true) {
                veganImageView.setColorFilter(
                    ContextCompat.getColor(requireContext(), R.color.green)
                )
                veganTextView.setTextColor(
                    ContextCompat.getColor(requireContext(), R.color.green)
                )
            }

            if (myBundle?.glutenFree == true) {
                glutenFreeImageView.setColorFilter(
                    ContextCompat.getColor(requireContext(), R.color.green)
                )
                glutenFreeTextView.setTextColor(
                    ContextCompat.getColor(requireContext(), R.color.green)
                )
            }

            if (myBundle?.dairyFree == true) {
                dairyFreeImageView.setColorFilter(
                    ContextCompat.getColor(requireContext(), R.color.green)
                )
                dairyFreeTextView.setTextColor(
                    ContextCompat.getColor(requireContext(), R.color.green)
                )
            }

            if (myBundle?.veryHealthy == true) {
                healthyImageView.setColorFilter(
                    ContextCompat.getColor(requireContext(), R.color.green)
                )
                healthyTextView.setTextColor(
                    ContextCompat.getColor(requireContext(), R.color.green)
                )
            }

            if (myBundle?.cheap == true) {
                cheapImageView.setColorFilter(
                    ContextCompat.getColor(requireContext(), R.color.green)
                )
                cheapTextView.setTextColor(
                    ContextCompat.getColor(requireContext(), R.color.green)
                )
            }
        }
    }
}
