package com.example.recipes.repository

import javax.inject.Inject

/* make the API call */
class RemoteDataSource @Inject constructor(
    private val foodRecipesApi: FoodRecipesApi
) {
    suspend fun getRecipesFromRemote(queries: Map<String, String>) =
        foodRecipesApi.getRecipesApi(queries)

    suspend fun searchRecipesFromRemote(searchQuery: Map<String, String>) =
        foodRecipesApi.searchRecipes(searchQuery)

    suspend fun getFoodJokeFromRemote(apiKey: String) =
        foodRecipesApi.getFoodJoke(apiKey)
}