package com.example.recipes.repository

import dagger.hilt.android.scopes.ActivityRetainedScoped
import javax.inject.Inject

/* survives configuration changes e.g: landscape mode */
@ActivityRetainedScoped
class Repository @Inject constructor(
    remoteDataSource: RemoteDataSource,
    localDataSource: LocalDataSource
) {
    /* indicates that the data is coming from the API */
    val remote = remoteDataSource

    /* indicates that the data is coming from th DB */
    val local = localDataSource
}