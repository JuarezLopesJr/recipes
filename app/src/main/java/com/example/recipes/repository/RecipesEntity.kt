package com.example.recipes.repository

import androidx.room.Entity
import androidx.room.PrimaryKey
import com.example.recipes.model.FoodRecipe
import com.example.recipes.utils.Constants.Companion.RECIPES_TABLE

@Entity(tableName = RECIPES_TABLE)
data class RecipesEntity(
    var foodRecipe: FoodRecipe
) {
    @PrimaryKey(autoGenerate = false)
    var id: Int = 0
}
