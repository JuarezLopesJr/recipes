package com.example.recipes.repository

import kotlinx.coroutines.flow.Flow
import javax.inject.Inject

/* manipulates the DB */
class LocalDataSource @Inject constructor(
    private val recipesDao: RecipesDao
) {

    fun insertRecipesFromLocal(recipesEntity: RecipesEntity) {
        recipesDao.insertRecipesDao(recipesEntity)
    }

    fun readRecipesFromLocal(): Flow<List<RecipesEntity>> = recipesDao.readRecipesDao()

    fun insertFavoriteRecipeFromLocal(favoritesEntity: FavoritesEntity) {
        recipesDao.insertFavoriteRecipeDao(favoritesEntity)
    }

    fun readFavoriteRecipeFromLocal(): Flow<List<FavoritesEntity>> =
        recipesDao.readFavoriteRecipesDao()

    fun deleteFavoriteRecipeFromLocal(favoritesEntity: FavoritesEntity) {
        recipesDao.deleteFavoriteRecipeDao(favoritesEntity)
    }

    fun deleteAllFavoriteRecipesFromLocal() {
        recipesDao.deleteAllFavoriteRecipesDao()
    }

    fun insertFoodJokeFromLocal(foodJoke: FoodJokeEntity) {
        recipesDao.insertFoodJokeDao(foodJoke)
    }

    fun readFoodJokesFromLocal(): Flow<List<FoodJokeEntity>> =
        recipesDao.readFoodJokesDao()
}