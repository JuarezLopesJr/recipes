package com.example.recipes.repository

import android.content.Context
import androidx.datastore.core.DataStore
import androidx.datastore.preferences.core.Preferences
import androidx.datastore.preferences.core.booleanPreferencesKey
import androidx.datastore.preferences.core.edit
import androidx.datastore.preferences.core.emptyPreferences
import androidx.datastore.preferences.core.intPreferencesKey
import androidx.datastore.preferences.core.stringPreferencesKey
import androidx.datastore.preferences.preferencesDataStore
import com.example.recipes.utils.Constants.Companion.DEFAULT_DIET_TYPE
import com.example.recipes.utils.Constants.Companion.DEFAULT_MEAL_TYPE
import com.example.recipes.utils.Constants.Companion.PREFERENCES_BACK_ONLINE
import com.example.recipes.utils.Constants.Companion.PREFERENCES_DIET_TYPE
import com.example.recipes.utils.Constants.Companion.PREFERENCES_DIET_TYPE_ID
import com.example.recipes.utils.Constants.Companion.PREFERENCES_MEAL_TYPE
import com.example.recipes.utils.Constants.Companion.PREFERENCES_MEAL_TYPE_ID
import com.example.recipes.utils.Constants.Companion.PREFERENCES_NAME
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.android.scopes.ActivityRetainedScoped
import java.io.IOException
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.catch
import kotlinx.coroutines.flow.map
import javax.inject.Inject

/* to survive configurations changes */
@ActivityRetainedScoped
class DataStoreRepository @Inject constructor(
    @ApplicationContext private val context: Context
) {

    private val Context.dataStore:
            DataStore<Preferences> by preferencesDataStore(name = PREFERENCES_NAME)

    private object PreferenceKeys {
        val selectedMealType = stringPreferencesKey(PREFERENCES_MEAL_TYPE)
        val selectedMealTypeId = intPreferencesKey(PREFERENCES_MEAL_TYPE_ID)
        val selectedDietType = stringPreferencesKey(PREFERENCES_DIET_TYPE)
        val selectedDietTypeId = intPreferencesKey(PREFERENCES_DIET_TYPE_ID)
        val backOnline = booleanPreferencesKey(PREFERENCES_BACK_ONLINE)
    }

    suspend fun saveMealAndDietTypeRepository(
        mealType: String,
        mealTypeId: Int,
        dietType: String,
        dietTypeId: Int
    ) {
        context.dataStore.edit {
            it[PreferenceKeys.selectedMealType] = mealType
            it[PreferenceKeys.selectedMealTypeId] = mealTypeId
            it[PreferenceKeys.selectedDietType] = dietType
            it[PreferenceKeys.selectedDietTypeId] = dietTypeId
        }
    }

    val readMealAndDietTypeRepository: Flow<MealAndDietType> =
        context.dataStore.data.catch { exception ->
            if (exception is IOException) {
                emit(emptyPreferences())
            } else {
                throw exception
            }
        }
            .map {
                MealAndDietType(
                    it[PreferenceKeys.selectedMealType] ?: DEFAULT_MEAL_TYPE,
                    it[PreferenceKeys.selectedMealTypeId] ?: 0,
                    it[PreferenceKeys.selectedDietType] ?: DEFAULT_DIET_TYPE,
                    it[PreferenceKeys.selectedDietTypeId] ?: 0
                )
            }

    suspend fun saveBackOnlineRepository(state: Boolean) {
        context.dataStore.edit {
            it[PreferenceKeys.backOnline] = state
        }
    }

    val readBackOnlineRepository: Flow<Boolean> =
        context.dataStore.data.catch { exception ->
            if (exception is IOException) {
                emit(emptyPreferences())
            } else {
                throw exception
            }
        }.map {
            val backOnline = it[PreferenceKeys.backOnline] ?: false
            backOnline
        }

    data class MealAndDietType(
        val selectedMealType: String,
        val selectedMealTypeId: Int,
        val selectedDietType: String,
        val selectedDietTypeId: Int
    )
}