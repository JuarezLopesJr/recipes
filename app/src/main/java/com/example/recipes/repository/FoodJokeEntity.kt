package com.example.recipes.repository

import androidx.room.Embedded
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.example.recipes.model.FoodJoke
import com.example.recipes.utils.Constants.Companion.FOOD_JOKE_TABLE

@Entity(tableName = FOOD_JOKE_TABLE)
data class FoodJokeEntity(
    @Embedded
    var result: FoodJoke
) {
    @PrimaryKey(autoGenerate = false)
    var id: Int = 0
}
