package com.example.recipes.repository

import androidx.room.Entity
import androidx.room.PrimaryKey
import com.example.recipes.model.RecipeResult
import com.example.recipes.utils.Constants.Companion.FAVORITE_RECIPES_TABLE

@Entity(tableName = FAVORITE_RECIPES_TABLE)
data class FavoritesEntity(
    @PrimaryKey(autoGenerate = true)
    var id: Int,
    var result: RecipeResult
)
